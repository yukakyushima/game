package com.exercise.dao;

import com.exercise.exception.GameException;
import com.exercise.model.Game;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GameDAOTests {

    @Test
    void shouldCreateGame() {
        Game newGame = new Game("Game 1", LocalDate.now(), false);

        GameDAO dao = new GameDAO();
        dao.add(newGame);

        List<Game> games = dao.getAll();
        assertEquals(1, games.size());
        assertEquals(newGame, games.get(0));
    }

    @Test
    void shouldThrowExceptionWhenCreatingAnExistingGameName() {
        Game game1 = new Game("Game 1", LocalDate.now(), false);
        Game game2 = new Game("Game 1", LocalDate.now(), true);
        GameDAO dao = new GameDAO();
        dao.add(game1);

        Exception exception = assertThrows(GameException.class, () -> {
            dao.add(game2);
        });

        String expectedMessage = "Game Game 1 already exists";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldUpdateGame() {
        Game newGame = new Game("Game 1", LocalDate.now(), false);

        GameDAO dao = new GameDAO();
        dao.add(newGame);

        dao.active(newGame.getName(), true);

        List<Game> games = dao.getAll();

        Game actualGame = games.get(0);
        assertEquals(newGame.getName(), actualGame.getName());
        assertEquals(newGame.getCreatedAt(), actualGame.getCreatedAt());
        assertTrue(actualGame.isActive());
    }

    @Test
    void shouldThrowExceptionWhenUpdatingAnAbsentGame() {

        GameDAO dao = new GameDAO();

        Exception exception = assertThrows(GameException.class, () -> {
            dao.active("Game 1", false);
        });

        String expectedMessage = "Game Game 1 does not exist";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldDeleteGame() {
        Game newGame = new Game("Game 1", LocalDate.now(), false);

        GameDAO dao = new GameDAO();
        dao.add(newGame);

        dao.delete(newGame.getName());

        List<Game> games = dao.getAll();
        assertEquals(0, games.size());
    }

    @Test
    void shouldThrowExceptionWhenDeletingAnAbsentGame() {
        GameDAO dao = new GameDAO();

        Exception exception = assertThrows(GameException.class, () -> {
            dao.delete("Game 1");
        });

        String expectedMessage = "Game Game 1 does not exist";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldGetGame() {
        Game newGame = new Game("Game 1", LocalDate.now(), false);

        GameDAO dao = new GameDAO();
        dao.add(newGame);

        Game actualGame = dao.get(newGame.getName());

        assertEquals(newGame, actualGame);
    }
}
