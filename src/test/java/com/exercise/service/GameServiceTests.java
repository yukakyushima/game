package com.exercise.service;

import com.exercise.dao.GameDAO;
import com.exercise.exception.GameException;
import com.exercise.model.Game;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(GameService.class)
public class GameServiceTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GameDAO dao;

    @Test
    void shouldGetGame() throws Exception {

        when(dao.get(any())).thenReturn(new Game("game1", LocalDate.of(2023, 7, 25), false));

        mockMvc.perform(get("/v1/game/game1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{ \"name\": \"game1\", \"createdAt\": \"2023-07-25\",  \"active\": false }", true));
    }

    @Test
    void shouldCreateGame() throws Exception {
        Game newGame = new Game("game1", LocalDate.of(2023, 7, 21), false);

        try (MockedStatic<LocalDate> mockClock = mockStatic(LocalDate.class, Mockito.CALLS_REAL_METHODS)) {
            LocalDate currentDate = LocalDate.of(2023, 7, 21);
            mockClock.when(LocalDate::now).thenReturn(currentDate);
            mockMvc.perform(
                    MockMvcRequestBuilders.post("/v1/game")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"name\": \"game1\",  \"active\": false }"))
                    .andExpect(status().isCreated())
                    .andExpect(content().string("Game game1 created"));

            verify(dao, times(1)).add(newGame);
        }
    }

    @Test
    void shouldNotCreateGame() throws Exception {
        Game newGame = new Game("game1", LocalDate.of(2023, 7, 21), false);
        when(dao.add(newGame)).thenThrow(GameException.class);

        try (MockedStatic<LocalDate> mockClock = mockStatic(LocalDate.class, Mockito.CALLS_REAL_METHODS)) {
            LocalDate currentDate = LocalDate.of(2023, 7, 21);
            mockClock.when(LocalDate::now).thenReturn(currentDate);
            mockMvc.perform(
                    MockMvcRequestBuilders.post("/v1/game")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"name\": \"game1\",  \"active\": false }"))
                    .andExpect(status().isBadRequest());

            verify(dao, times(1)).add(newGame);
        }
    }

    @Test
    void shouldUpdateGame() throws Exception {
        mockMvc.perform(put("/v1/game/game1?active=true"))
                .andExpect(status().isOk())
                .andExpect(content().string("Game game1 updated"));

        verify(dao, times(1)).active("game1", true);
    }

    @Test
    void shouldDeleteGame() throws Exception {
        mockMvc.perform(delete("/v1/game/game1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Game game1 deleted"));

        verify(dao, times(1)).delete("game1");
    }
}
