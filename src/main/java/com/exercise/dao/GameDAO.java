package com.exercise.dao;

import com.exercise.exception.GameException;
import com.exercise.model.Game;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class GameDAO {

    private final Map<String, Game> cache = new ConcurrentHashMap<>();

    public Game get(String name) {
        return cache.get(name);
    }

    public synchronized Game add(Game newGame) {
        if (cache.containsKey(newGame.getName())) {
            throw new GameException(String.format("Game %s already exists", newGame.getName()));
        }
        cache.put(newGame.getName(), newGame);
        return newGame;
    }

    public synchronized void active(String name, boolean active) {
        if (!cache.containsKey(name)) {
            throw new GameException(String.format("Game %s does not exist", name));
        }
        cache.get(name).setActive(active);
    }

    public synchronized void delete(String name) {
        if (!cache.containsKey(name)) {
            throw new GameException(String.format("Game %s does not exist", name));
        }
        cache.remove(name);
    }

    public List<Game> getAll() {
        return cache.values().stream().toList();
    }
}
