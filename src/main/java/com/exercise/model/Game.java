package com.exercise.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Game {

    private String name;
    private LocalDate createdAt;
    private boolean active;

    public Game(String name, LocalDate createdAt, boolean active) {
        this.name = name;
        this.createdAt = createdAt;
        this.active = active;
    }
}
