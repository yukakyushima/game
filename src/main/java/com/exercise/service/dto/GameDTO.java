package com.exercise.service.dto;

import lombok.Data;

@Data
public class GameDTO {

    private String name;
    private boolean active;
}
