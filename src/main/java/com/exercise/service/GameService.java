package com.exercise.service;

import com.exercise.exception.GameException;
import com.exercise.dao.GameDAO;
import com.exercise.model.Game;
import com.exercise.service.dto.GameDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/v1/game")
@Slf4j
@Tag(name = "game", description = "handle game-related operations (crud)")
public class GameService {

    @Autowired
    private GameDAO dao;

    @Operation(summary = "Get a game by its name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the game",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Game.class)) })})
    @GetMapping("{name}")
    public Game get(@Parameter(description = "The game name that needs to be fetched") @PathVariable("name") String name) {
        log.info("Getting game {}", name);
        return dao.get(name);
    }

    @Operation(summary = "Create a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Game created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Game name already exists", content = @Content)})
    @PostMapping
    public ResponseEntity<String> create(@RequestBody GameDTO gameDTO) {
        log.info("Creating game {}", gameDTO.getName());
        Game newGame = new Game(gameDTO.getName(), LocalDate.now(), gameDTO.isActive());

        try {
            dao.add(newGame);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(String.format("Game %s created", gameDTO.getName()));
        } catch (GameException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @Operation(summary = "Change active status of a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game updated", content = @Content),
            @ApiResponse(responseCode = "400", description = "Game name does not exist", content = @Content)})
    @PutMapping("{name}")
    public ResponseEntity<String> update(@Parameter(description = "The game name that needs to be updated") @PathVariable("name") String name, @Parameter(description = "The active status to update the game") @RequestParam Boolean active) {
        log.info("Updating game {}", name);
        try {
            dao.active(name, active);
            return ResponseEntity.ok(String.format("Game %s updated", name));
        } catch (GameException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @Operation(summary = "Remove a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game deleted", content = @Content),
            @ApiResponse(responseCode = "400", description = "Game name does not exist", content = @Content)})
    @DeleteMapping("{name}")
    public ResponseEntity<String> delete(@Parameter(description = "The game name that needs to be deleted") @PathVariable("name") String name) {
        log.info("Deleting game {}", name);
        try {
            dao.delete(name);
            return ResponseEntity.ok(String.format("Game %s deleted", name));
        } catch (GameException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
