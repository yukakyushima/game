# Game

## Description
Simple REST API to handle game related operation (crud)

## Installation

Run the bellow commands to install and run the application:
```
mvn clean install
./game
```

## Documentation
```
http://localhost:8080/swagger-ui/index.html
```
